<?php
session_start();
require_once 'config.php';

if (!isset($_SESSION['username'])) {
    header('Location: login.php');
    exit;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = $_SESSION['username'];
    $content = $_POST['content'];
    $color = $_POST['color'];

    try {
        $stmt = $pdo->prepare("INSERT INTO wishes (username, content, color) VALUES (:username, :content, :color)");
        $stmt->execute(['username' => $username, 'content' => $content, 'color' => $color]);
        header('Location: index.php');
        exit;
    } catch (PDOException $e) {
        die('数据库插入错误: ' . $e->getMessage());
    }
}
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>添加愿望</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-image: url("https://img-baofun.zhhainiao.com/pcwallpaper_ugc/static/84a7753e8962cf921aee34c6eb0839b6.jpg?x-oss-process=image%2fresize%2cm_lfit%2cw_960%2ch_540");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        .container {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            width: 300px;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        label {
            display: block;
            margin-bottom: 5px;
            color: #555;
        }
        textarea, select {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        button {
            width: 100%;
            padding: 10px;
            background-color: #007BFF;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s;
        }
        button:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>添加愿望</h1>
        <form method="post">
            <label for="content">愿望内容:</label>
            <textarea id="content" name="content" required></textarea><br>
            <label for="color">颜色:</label>
            <select id="color" name="color">
                <option value="default">默认</option>
                <option value="red">红色</option>
                <option value="blue">蓝色</option>
                <option value="green">绿色</option>
            </select><br>
            <button type="submit">添加愿望</button>
        </form>
    </div>
</body>
</html>