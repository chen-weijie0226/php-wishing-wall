<?php
session_start();
require_once 'config.php';

if (!isset($_SESSION['username'])) {
    header('Location: login.php');
    exit;
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    try {
        $stmt = $pdo->prepare("SELECT * FROM wishes WHERE id = :id");
        $stmt->execute(['id' => $id]);
        $wish = $stmt->fetch();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $content = $_POST['content'];
            $stmt = $pdo->prepare("UPDATE wishes SET content = :content WHERE id = :id");
            $stmt->execute(['content' => $content, 'id' => $id]);
            header('Location: index.php');
            exit;
        }
    } catch (PDOException $e) {
        die('数据库查询错误: ' . $e->getMessage());
    }
} else {
    header('Location: index.php');
    exit;
}
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>修改愿望</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-image: url("https://pic3.zhimg.com/v2-230b33bf409772ea9a62be47d25cf816_r.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        .container {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            width: 100%;
            max-width: 400px;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        label {
            display: block;
            margin-bottom: 8px;
            color: #555;
        }
        textarea {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            margin-bottom: 20px;
            resize: none;
            height: 150px;
        }
        button {
            background-color: #007BFF;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }
        button:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>修改愿望</h1>
        <form method="post">
            <label for="content">愿望内容:</label>
            <textarea id="content" name="content" required><?php echo htmlspecialchars($wish['content']); ?></textarea><br>
            <button type="submit">保存修改</button>
        </form>
    </div>
</body>
</html>