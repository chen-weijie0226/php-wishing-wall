<?php
session_start();
require_once 'config.php';

if (!isset($_SESSION['username'])) {
    header('Location: login.php');
    exit;
}

try {
    $stmt = $pdo->query("SELECT * FROM wishes");
    $wishes = $stmt->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    die('数据库查询错误: ' . $e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title>许愿墙</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            background-image: url("https://pic3.zhimg.com/v2-217f1b1062ab037739e18c823aa15db6_r.jpg");
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center;
        }
        h1 {
            text-align: center;
            color: #333;
        }
        a {
            text-decoration: none;
            color: #007BFF;
        }
        a:hover {
            text-decoration: underline;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
        }
        .wish {
            border: 1px solid #ccc;
            padding: 10px;
            margin: 10px 0;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }
        .wish p {
            margin: 0;
        }
        .wish strong {
            color: #333;
        }
        .wish small {
            color: #999;
        }
        .actions a {
            margin-right: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>许愿墙</h1>
        <a href="logout.php">退出登录</a>
        <a href="add_wish.php">添加愿望</a>
        <div id="wishes">
            <?php foreach ($wishes as $wish): ?>
                <div class="wish">
                    <p><strong><?php echo htmlspecialchars($wish['username']); ?>:</strong> <?php echo htmlspecialchars($wish['content']); ?></p>
                    <p><small><?php echo $wish['create_time']; ?></small></p>
                    <div class="actions">
                        <a href="edit_wish.php?id=<?php echo $wish['id']; ?>">修改</a>
                        <a href="delete_wish.php?id=<?php echo $wish['id']; ?>">删除</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</body>
</html>