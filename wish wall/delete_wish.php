<?php
session_start();
require_once 'config.php';

if (!isset($_SESSION['username'])) {
    header('Location: login.php');
    exit;
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    try {
        $stmt = $pdo->prepare("DELETE FROM wishes WHERE id = :id");
        $stmt->execute(['id' => $id]);
        header('Location: index.php');
        exit;
    } catch (PDOException $e) {
        die('数据库删除错误: ' . $e->getMessage());
    }
} else {
    header('Location: index.php');
    exit;
}
?>